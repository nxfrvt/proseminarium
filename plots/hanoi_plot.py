import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
import scipy.optimize as opt
from time import perf_counter_ns
import os

from algorithms import hanoi_tower as ht


def func(x, a, b):
    return a * (2 ** x) + b


def measure_time(size, recursive=False):
    A = ht.generate_data(size)
    B = []
    C = []
    if recursive:
        start = perf_counter_ns()
        ht.hanoi_rec(size, A, B, C)
        end = perf_counter_ns()
    else:
        start = perf_counter_ns()
        ht.hanoi_iter(size, A, B, C)
        end = perf_counter_ns()
    return (end - start) * 1E-9


def hanoi_plot(n):
    x = np.arange(1, n + 1)
    xf = np.arange(1, n + 1, 0.01)
    y_rec = np.zeros(n)
    y_iter = np.zeros(n)
    for i in x:
        y_rec[i - 1] = measure_time(i, recursive=True)
        y_iter[i - 1] = measure_time(i, recursive=False)
    param_rec, pcov_rec = opt.curve_fit(func, x, y_rec)
    param_iter, pcov_iter = opt.curve_fit(func, x, y_iter)

    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(plticker.MultipleLocator(base=1))
    ax.xaxis.set_minor_locator(plticker.MultipleLocator(base=1))

    plt.plot(x, y_iter, 'bo', markersize=4, label="Iteracyjnie")
    plt.plot(x, y_rec, 'ro', markersize=4, label="Rekurencyjnie")
    plt.plot(xf, func(xf, *param_iter), 'b',
             label="y = " + '{:0.3e}'.format(param_iter[0]) + " * 2^n + " + '{:0.3e}'.format(param_iter[1]))
    plt.plot(xf, func(xf, *param_rec), 'r',
             label="y = " + '{:0.3e}'.format(param_rec[0]) + " * 2^n + " + '{:0.3e}'.format(param_rec[1]))

    plt.xlabel('Ilość krążków')
    plt.ylabel('Czas [s]')
    plt.grid(True)
    plt.legend()
    plt.title("Wieże Hanoi")
    plt.show()

    fig.savefig(os.path.dirname(os.path.dirname(__file__)) + '/hanoi_tower.png', dpi=fig.dpi * 3)


if __name__ == '__main__':
    hanoi_plot(15)
