# Python implemenation of selected sorting algorithms

## Table of contets
* [About the project](#about-the-project)
* [Built with](#built-with)
* [Authors](#authors)

## About the project
Implementation of sorting algorithms with two different approaches: recursion and iteration.

Algorithms implemented as a part of the project:
- Tower of Hanoi
- Mergesort
- Quicksort

## Built with
<li><a href="https://www.python.org">Python</a>
<ul>
<li><a href="#packages">matplotlib</a></li>
<li><a href="#packages">scipy</a></li></li>

## Authors
* [Bartłomiej Mazurek](https://gitlab.com/nxfrvt)
* [Katarzyna Trzcionka](https://gitlab.com/Lvsia)
* [Wojciech Radzimowski](https://gitlab.com/airflame)
