#!/usr/bin/env python3
from random import shuffle
from time import perf_counter_ns


def generate_sequence(n):
    numbers = list(range(1, n+1))
    shuffle(numbers)
    return numbers


def mergesort_rec(array: []):
    """
    Sorts provided array using recursive implementation of mergesort.
    :param array: array to be sorted in form of array list
    """
    if len(array) > 1:
        middle = len(array) // 2

        left_part = array[:middle]
        right_part = array[middle:]

        mergesort_rec(left_part)
        mergesort_rec(right_part)

        left_index = right_index = main_index = 0

        while left_index < len(left_part) and right_index < len(right_part):
            if left_part[left_index] < right_part[right_index]:
                array[main_index] = left_part[left_index]
                left_index += 1
            else:
                array[main_index] = right_part[right_index]
                right_index += 1
            main_index += 1

        while left_index < len(left_part):
            array[main_index] = left_part[left_index]
            left_index += 1
            main_index += 1

        while right_index < len(right_part):
            array[main_index] = right_part[right_index]
            right_index += 1
            main_index += 1


def mergesort_it(array: []):
    """
    Sorts provided array using iterative implementation of mergesort.
    :param array: array to be sorted in form of array list
    """
    current_size = 1
    while current_size < len(array) - 1:
        left = 0
        while left < len(array) - 1:
            middle = min((left + current_size - 1), (len(array) - 1))

            right = ((2 * current_size + left - 1,
                      len(array) - 1)[2 * current_size
                                      + left - 1 > len(array) - 1])

            # Merging here
            left_size = middle - left + 1
            right_size = right - middle
            left_part = [0] * left_size
            right_part = [0] * right_size

            for i in range(0, left_size):
                left_part[i] = array[left + i]
            for i in range(0, right_size):
                right_part[i] = array[middle + i + 1]

            left_index, right_index, main_index = 0, 0, left
            while left_index < left_size and right_index < right_size:
                if left_part[left_index] > right_part[right_index]:
                    array[main_index] = right_part[right_index]
                    right_index += 1
                else:
                    array[main_index] = left_part[left_index]
                    left_index += 1
                main_index += 1

            while left_index < left_size:
                array[main_index] = left_part[left_index]
                left_index += 1
                main_index += 1

            while right_index < right_size:
                array[main_index] = right_part[right_index]
                right_index += 1
                main_index += 1
            left = left + current_size * 2

        current_size = 2 * current_size


def mergesort(size: int, tell: bool):
    data = generate_sequence(size)
    if tell:
        if size > 100:
            print(f"Randomly generated list to be sorted: \n{data[:5]}...{data[size - 5:]}\n")
        else:
            print(f"Randomly generated list to be sorted: \n{data}\n")

    start = perf_counter_ns()
    mergesort_rec(data)
    end = perf_counter_ns()

    if tell:
        print(f"Recursive Mergesort time for {size} numbers: {end-start} ns")
        if size > 100:
            print(f"Sorted list: \n{data[:5]}...{data[size-5:]}\n")
        else:
            print(f"Sorted list: \n{data}\n")
    else:
        rc_time = end-start

    data = generate_sequence(size)
    if tell:
        if size > 100:
            print(f"Randomly generated list to be sorted: \n{data[:5]}...{data[size - 5:]}\n")
        else:
            print(f"Randomly generated list to be sorted: \n{data}\n")

    start = perf_counter_ns()
    mergesort_it(data)
    end = perf_counter_ns()

    if tell:
        print(f"Iterative Mergesort time for {size} numbers: {end-start} ns")
        if size > 100:
            print(f"Sorted list: \n{data[:5]}...{data[size - 5:]}")
        else:
            print(f"Sorted list: \n{data}")
    else:
        it_time = end-start
        return (rc_time, it_time)


if __name__ == '__main__':
    pass
