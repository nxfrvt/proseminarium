from algorithms.hanoi_tower import hanoi_tower
from algorithms.mergesort import mergesort
from algorithms.quicksort import quicksort
from plots.hanoi_plot import hanoi_plot
from plots.mergesort_plot import mergesort_plot
from plots.quicksort_plot import quicksort_plot
import sys


def submenu_timers():
    print("===============================================\n"
          "\t\t\t\tEXECUTION TIME\n"
          "===============================================\n")
    x = int(input("1. Tower of Hanoi\n2. Mergesort\n3. Quicksort\n4. Back\n"))
    if x == 4:
        pass
    else:
        size = int(input("Size: "))
        if x == 1:
            hanoi_tower(size, True)
        elif x == 2:
            mergesort(size, True)
        elif x == 3:
            quicksort(size, True)


def submenu_plots():
    print("===============================================\n"
          "\t\t\t\t\tPLOTS\n"
          "===============================================\n")
    x = int(input("1. Tower of Hanoi\n2. Mergesort\n3. Quicksort\n4. Back\n"))
    if x == 4:
        pass
    else:
        size = int(input("Size: "))
        if x == 1:
            hanoi_plot(size)
        elif x == 2:
            mergesort_plot(size)
        elif x == 3:
            quicksort_plot(size)


def menu():
    print("===============================================\n"
          "\t\t\t  SORTING ALGORITHMS\n"
          "===============================================\n")
    x = int(input("1. Timing\n2. Plots\n3. Exit\n"))
    if x == 1:
        submenu_timers()
    elif x == 2:
        submenu_plots()
    elif x == 3:
        sys.exit()


if __name__ == '__main__':
    while True:
        menu()








