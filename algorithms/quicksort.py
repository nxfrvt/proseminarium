#!/usr/bin/env python3
import random
from time import perf_counter_ns


def generate_sequence(n):
    numbers = list(range(1, n+1))
    random.shuffle(numbers)
    return numbers


def quicksort_rec(sequence):
    length = len(sequence)
    if length < 2:
        return sequence
    else:
        pivot = sequence.pop()
    numbers_greater = []
    numbers_lower = []
    for number in sequence:
        if number > pivot:
            numbers_greater.append(number)
        else:
            numbers_lower.append(number)
    return quicksort_rec(numbers_lower) + [pivot] + quicksort_rec(numbers_greater)


def partition(numbers_list, low_index, high_index):
    divider = low_index
    pivot_index = high_index
    for i in range(low_index, high_index):
        if numbers_list[i] < numbers_list[pivot_index]:
            numbers_list[i], numbers_list[divider] = numbers_list[divider], numbers_list[i]
            divider += 1
    numbers_list[pivot_index], numbers_list[divider] = numbers_list[divider], numbers_list[pivot_index]
    return divider


def quicksort_iter(sequence):
    if len(sequence) < 2:
        return sequence
    else:
        stack = [0] * len(sequence)
        low_index = 0
        high_index = len(sequence) - 1
        top = 0
        stack[top] = low_index
        top += 1
        stack[top] = high_index
        while top >= 0:
            high_index = stack[top]
            top -= 1
            low_index = stack[top]
            top -= 1
            pivot_index = partition(sequence, low_index, high_index)
            if pivot_index - 1 > low_index:
                top += 1
                stack[top] = low_index
                top += 1
                stack[top] = pivot_index - 1
            if pivot_index + 1 < high_index:
                top += 1
                stack[top] = pivot_index + 1
                top += 1
                stack[top] = high_index


def quicksort(size: int, tell: bool):
    data = generate_sequence(size)
    if tell:
        if size > 100:
            print(f"Randomly generated list to be sorted: \n{data[:5]}...{data[size - 5:]}\n")
        else:
            print(f"Randomly generated list to be sorted: \n{data}\n")

    start = perf_counter_ns()
    data = quicksort_rec(data)
    end = perf_counter_ns()

    if tell:
        print(f"Recursive Quicksort time for {size} numbers: {end-start} ns")
        if size > 100:
            print(f"Sorted list: \n{data[:5]}...{data[size-5:]}\n")
        else:
            print(f"Sorted list: \n{data}\n")
    else:
        rc_time = end-start

    data = generate_sequence(size)
    if tell:
        if size > 100:
            print(f"Randomly generated list to be sorted: \n{data[:5]}...{data[size - 5:]}\n")
        else:
            print(f"Randomly generated list to be sorted: \n{data}\n")

    start = perf_counter_ns()
    quicksort_iter(data)
    end = perf_counter_ns()

    if tell:
        print(f"Iterative Quicksort time for {size} numbers: {end-start} ns")
        if size > 100:
            print(f"Sorted list: \n{data[:5]}...{data[size-5:]}\n")
        else:
            print(f"Sorted list: \n{data}\n")
    else:
        it_time = end-start
        return (rc_time, it_time)


if __name__ == '__main__':
    pass
