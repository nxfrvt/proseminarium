#!/usr/bin/env python3
from time import perf_counter_ns
import time


def generate_data(n):
    return list(range(1, n + 1))[::-1]


def move(src, dest):
    dest.append(src.pop())


def hanoi_rec(n, src, dest, aux):
    if n == 1:
        move(src, dest)
        return
    hanoi_rec(n - 1, src, aux, dest)
    move(src, dest)
    hanoi_rec(n - 1, aux, dest, src)


def hanoi_iter(n, src, dest, aux):
    max = 2**n - 1
    if n % 2 == 0:
        temp = dest
        dest = aux
        aux = temp
    for i in range(1, max + 1):
        if i % 3 == 1:
            if len(src) == 0:
                move(dest, src)
            elif len(dest) == 0:
                move(src, dest)
            elif src[-1] > dest[-1]:
                move(dest, src)
            else:
                move(src, dest)
        if i % 3 == 2:
            if len(src) == 0:
                move(aux, src)
            elif len(aux) == 0:
                move(src, aux)
            elif src[-1] > aux[-1]:
                move(aux, src)
            else:
                move(src, aux)
        if i % 3 == 0:
            if len(aux) == 0:
                move(dest, aux)
            elif len(dest) == 0:
                move(aux, dest)
            elif aux[-1] > dest[-1]:
                move(dest, aux)
            else:
                move(aux, dest)


def hanoi_tower(size: int, tell: bool):
    A = generate_data(size)
    B = []
    C = []
    if tell:
        print(f"Randomly generated tower of Hanoi: \nA: {A}\nB: {B}\nC: {C}\n")

    start = perf_counter_ns()
    hanoi_rec(size, A, B, C)
    end = perf_counter_ns()

    if tell:
        print(f"Recursive solution for Tower of Hanoi time for {size} numbers: {end-start} ns")
        print(f"Solved tower of Hanoi: \nA: {A}\nB: {B}\nC: {C}\n")
    else:
        rc_time = end-start

    A = generate_data(size)
    B = []
    C = []
    if tell:
        print(f"Randomly generated Hanoi tower: \nA: {A}\nB: {B}\nC: {C}\n")

    start = perf_counter_ns()
    hanoi_iter(size, A, B, C)
    end = perf_counter_ns()

    if tell:
        if tell:
            print(f"Iterative solution for Tower of Hanoi time for {size} numbers: {end - start} ns")
            print(f"Solved tower of Hanoi: \nA: {A}\nB: {B}\nC: {C}\n")
    else:
        it_time = end-start
        return (rc_time, it_time)


def x():
    for i in range(1, 20):
        A = generate_data(i)
        B = []
        C = []
        print(A, B, C)
        start = time.time()
        hanoi_rec(i, A, B, C)
        end = time.time()
        print(A, B, C)
        print("Rec", i)
        print(end - start)

        A = generate_data(i)
        B = []
        C = []
        print(A, B, C)
        start = time.time()
        hanoi_iter(i, A, B, C)
        end = time.time()
        print(A, B, C)
        print("Iter", i)
        print(end - start)
        print()


if __name__ == '__main__':
    hanoi_tower(5, True)
