import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
import scipy.optimize as opt
import os
from time import perf_counter_ns

from algorithms import quicksort as qt


def func(n, a, b):
    return a * (n * np.log(n)) + b


def measure_time(size, recursive=False):
    data = qt.generate_sequence(size)
    if recursive:
        start = perf_counter_ns()
        qt.quicksort_rec(data)
        end = perf_counter_ns()
    else:
        start = perf_counter_ns()
        qt.quicksort_iter(data)
        end = perf_counter_ns()
    return (end - start) * 1E-9


def quicksort_plot(n):
    x = np.arange(1, n + 1)
    xf = np.arange(1, n + 1, 0.01)
    y_rec = np.zeros(n)
    y_iter = np.zeros(n)
    for i in x:
        y_rec[i - 1] = measure_time(i, recursive=True)
        y_iter[i - 1] = measure_time(i, recursive=False)
    np.vectorize(func)
    param_rec, pcov_rec = opt.curve_fit(func, x, y_rec)
    param_iter, pcov_iter = opt.curve_fit(func, x, y_iter)

    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(plticker.MultipleLocator(base=1))
    ax.xaxis.set_minor_locator(plticker.MultipleLocator(base=1))

    plt.plot(x, y_iter, 'bo', markersize=4, label="Iteracyjnie")
    plt.plot(x, y_rec, 'ro', markersize=4, label="Rekurencyjnie")
    plt.plot(xf, func(xf, *param_iter), 'b',
             label="y = " + '{:0.3e}'.format(param_iter[0]) + " * n * log(n) + " + '{:0.3e}'.format(param_iter[1]))
    plt.plot(xf, func(xf, *param_rec), 'r',
             label="y = " + '{:0.3e}'.format(param_rec[0]) + " * n * log(n) + " + '{:0.3e}'.format(param_rec[1]))

    plt.xlabel('Rozmiar listy')
    plt.ylabel('Czas [s]')
    plt.grid(True)
    plt.legend()
    plt.title("Quicksort")
    plt.show()

    fig.savefig(os.path.dirname(os.path.dirname(__file__)) + '/quicksort_plot.png', dpi=fig.dpi * 3)


if __name__ == '__main__':
    quicksort_plot(25)
